#Define a imagem base como Node.js na versão 14.
FROM node:14 

#Define o diretório de trabalho dentro do contêiner como /app.
WORKDIR /app

#Copia o package.json e package-lock.json para o diretório de trabalho.
COPY package*.json ./

#Instala as dependências do aplicativo.
RUN npm install

#Copia todo o código-fonte para o diretório de trabalho.
COPY . .

#Informa ao Docker que o contêiner escuta na porta 3000.
EXPOSE 3000

#Define o comando padrão para iniciar o aplicativo quando o contêiner é iniciado.
CMD ["node", "app.js"]
